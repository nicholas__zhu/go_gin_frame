package utils

import (
	"gitee.com/nicholas__zhu/go_gin_frame/global"
	"sync"
)

// SafeGoAsync 异步执行，不影响线程返回结果
func SafeGoAsync(f []func()) {
	for i := 0; i < len(f); i++ {
		i := i
		go func() {
			defer func() {
				if err := recover(); err != nil {
					global.Log.Error("SafeGoAsync recover ", err)
				}
			}()
			f[i]()
		}()
	}
}

// SafeGoSync 同步执行，执行完成，返回结果 携带panic恢复
func SafeGoSync(f []func()) {
	wg := &sync.WaitGroup{}
	wg.Add(len(f))
	for i := 0; i < len(f); i++ {
		i := i
		go func() {
			defer func() {
				if err := recover(); err != nil {
					global.Log.Error("SafeGoSync recover ", err)
				}
			}()
			defer wg.Done()
			f[i]()
		}()
	}
	wg.Wait()
}
