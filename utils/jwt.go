package utils

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"gitee.com/nicholas__zhu/go_gin_frame/global"
	"strings"
	"time"
)

/* 用法详解
token, err := jwt.NewJWT("secret").Encode(
map[string]string{"alg": "HS256", "typ": "JWT"},
map[string]interface{}{"sub": "1234567890", "name": "John Doe", "iat": 1516239022}
)
    if err != nil {
        fmt.Println("Error:", err)
        return
    }
    fmt.Println("Token:", token)

    payload, err := jwt.NewJWT("secret").Decode(token)
    if err != nil {
        fmt.Println("Error:", err)
        return
    }
    fmt.Println("Payload:", payload)

*/

type JWT struct {
	secretKey []byte
}
type JWTHeader struct {
	Alg string `json:"alg"` // 项目名称
	Typ string `json:"typ"` //加密类型 shar
}

type JWTBody struct {
	NickName string `json:"nick_name"` //昵称
	UserId   string `json:"user_id"`   //用户id
	Iat      int64  `json:"iat"`       //过期时间 时间戳
}

const (
	Separator string = "#"
	Pre       string = "Bearer "
	HeaderKey string = "Authorization"
)

// NewJWT 初始化jwt
// param secret 秘钥
func NewJWT(secret string) *JWT {
	return &JWT{
		secretKey: []byte(secret),
	}
}

// Encode 生成秘钥
func (j *JWT) Encode(header JWTHeader, payload JWTBody) (string, error) {
	//处理过期时间
	addTime, err := ParseDuration(global.Config.JWT.ExpiresTime)
	if err != nil {
		return "", err
	}
	payload.Iat = time.Now().Add(addTime).Unix()

	// 处理 header
	headerBytes, err := json.Marshal(header)
	if err != nil {
		return "", err
	}
	headerEncoded := base64.URLEncoding.EncodeToString(headerBytes)

	// 处理 载荷编码
	payloadBytes, err := json.Marshal(payload)
	if err != nil {
		return "", err
	}
	payloadEncoded := base64.URLEncoding.EncodeToString(payloadBytes)

	// 计算 signature
	message := headerEncoded + Separator + payloadEncoded
	mac := hmac.New(sha256.New, j.secretKey)
	mac.Write([]byte(message))
	signatureEncoded := MD5(base64.URLEncoding.EncodeToString(mac.Sum(nil)))

	// 拼接 JWT
	return Pre + headerEncoded + Separator + payloadEncoded + Separator + signatureEncoded, nil
}

func (j *JWT) Decode(token string) (*JWTBody, error) {
	parts := strings.Split(token, Separator)
	if len(parts) != 3 {
		return nil, errors.New("token失效或者token不可用")
	}

	// 验证 signature
	message := parts[0] + Separator + parts[1]
	mac := hmac.New(sha256.New, j.secretKey)
	mac.Write([]byte(message))
	signatureEncoded := MD5(base64.URLEncoding.EncodeToString(mac.Sum(nil)))
	if signatureEncoded != parts[2] {
		return nil, errors.New("token失效或者token不可用")
	}

	// 处理 payload
	payloadBytes, err := base64.URLEncoding.DecodeString(parts[1])
	if err != nil {
		return nil, errors.New("token失效或者token不可用")
	}
	var payload = &JWTBody{}
	err = json.Unmarshal(payloadBytes, payload)
	if err != nil {
		global.Log.Error("token解析失败：err：", err)
		return nil, errors.New("token失效或者token不可用")
	}

	//查看token过期时间
	currentTime := time.Now().Unix()
	if payload.Iat < currentTime {
		return nil, errors.New("token已经失效")
	}

	return payload, nil
}
