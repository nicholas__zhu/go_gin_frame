/************************
 * Author : nicholas
 * Email  : nicholas.zhu01@gmail.com
 * DATE   : 2023/5/4
 ***********************/

// Package utils ******
package utils

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"gitee.com/nicholas__zhu/go_gin_frame/global"
	"io"
	"net/http"
	"strings"
	"time"
)

var client *http.Client

func init() {
	def := http.DefaultTransport
	defPot, ok := def.(*http.Transport)
	if !ok {
		global.Log.Error("init transport出错")
	}
	defPot.MaxIdleConns = 100
	defPot.MaxIdleConnsPerHost = 100
	defPot.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	client = &http.Client{
		Timeout:   time.Second * time.Duration(20),
		Transport: defPot,
	}
}

func Get(url string, header map[string]string, params map[string]interface{}) (string, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}
	if header != nil {
		for k, v := range header {
			req.Header.Add(k, v)
		}
	}

	q := req.URL.Query()
	if params != nil {
		for Key, val := range params {
			v, _ := toString(val)
			q.Add(Key, v)
		}
		req.URL.RawQuery = q.Encode()
	}

	r, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer func() {
		if r.Body != nil {
			_ = r.Body.Close()
		}
	}()
	if r.StatusCode != 200 {
		return "", err
	}
	bb, err := io.ReadAll(r.Body)
	if err != nil {
		return "", err
	}
	return string(bb), nil
}

func Post(url string, header map[string]string, param map[string]interface{}) (string, error) {
	jsonStr, _ := json.Marshal(param)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	if err != nil {
		return "", err
	}
	if header != nil {
		for k, v := range header {
			req.Header.Add(k, v)
		}
	}
	r, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer func() {
		if r.Body != nil {
			_ = r.Body.Close()
		}
	}()
	bb, err := io.ReadAll(r.Body)
	if err != nil {
		return "", err
	}
	return string(bb), nil
}

func PostJson(url string, header map[string]string, param map[string]interface{}) (string, error) {
	dd, _ := json.Marshal(param)
	re := bytes.NewReader(dd)
	req, err := http.NewRequest("POST", url, re)
	if err != nil {
		return "", err
	}
	if header != nil {
		for k, v := range header {
			req.Header.Add(k, v)
		}
	}
	r, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer func() {
		if r.Body != nil {
			_ = r.Body.Close()
		}
	}()
	bb, err := io.ReadAll(r.Body)
	if err != nil {
		return "", err
	}
	return string(bb), nil
}

func PostAny(url string, header map[string]string, re io.Reader) (string, error) {
	req, err := http.NewRequest("POST", url, re)
	if err != nil {
		return "", err
	}
	if header != nil {
		for k, v := range header {
			req.Header.Add(k, v)
		}
	}
	r, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer func() {
		if r.Body != nil {
			_ = r.Body.Close()
		}
	}()
	bb, err := io.ReadAll(r.Body)
	if err != nil {
		return "", err
	}
	return string(bb), nil
}

// BaiduPushApi 百度推送api
//
//	  百度推送API
//		 发送POST请求
//		 url：         请求地址：http://data.zz.baidu.com/urls?site=https://www.helloworldtools.com&token=xxxx
//		 data：        POST请求提交的数据(推送的连接)
//		 contentType： 请求体格式: text/plain
//		 成功返回示例
//		 {
//			 "remain":99998,
//			 "success":2,
//			 "not_same_site":[],
//			 "not_valid":[]
//		 }
func BaiduPushApi(url, data, contentType string) string {
	// 超时时间：5秒
	client := &http.Client{Timeout: 5 * time.Second}
	resp, err := client.Post(url, contentType, strings.NewReader(data))
	resp.Header.Add("User-Agent", "curl/7.12.1")
	resp.Header.Add("Host", "data.zz.baidu.com")
	resp.Header.Add("Content-Length", "83")
	if err != nil {
		global.Log.Error("推送数据失败 err：", err)
	}
	defer func() {
		if resp.Body != nil {
			_ = resp.Body.Close()
		}
	}()
	result, _ := io.ReadAll(resp.Body)
	return string(result)
}
