/************************
 * Author : nicholas
 * Email  : nicholas.zhu01@gmail.com
 * DATE   : 2023/5/4
 ***********************/

// Package utils ******
package utils

import (
	"crypto/md5"
	"crypto/sha1"
	"encoding/hex"
	"github.com/forgoer/openssl"
)

func SHA1(s string) string {
	o := sha1.New()
	o.Write([]byte(s))
	return hex.EncodeToString(o.Sum(nil))
}

func MD5(s string) string {
	d := []byte(s)
	m := md5.New()
	m.Write(d)
	return hex.EncodeToString(m.Sum(nil))
}

func OpensslDecode(data, key string) (string, error) {
	hexString, err := hex.DecodeString(data)
	if err != nil {
		return "", err
	}
	dst, err := openssl.AesECBDecrypt(hexString, []byte(key), openssl.PKCS7_PADDING)
	if err != nil {
		return "", err
	}
	return string(dst), nil
}

func OpensslEncode(data, key string) (string, error) {
	dst, err := openssl.AesECBEncrypt([]byte(data), []byte(key), openssl.PKCS7_PADDING)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(dst), nil
}
