package utils

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"time"
)

type LocalTime struct {
	time.Time
}

// MarshalJSON 时间格式化
func (t LocalTime) MarshalJSON() ([]byte, error) {
	//格式化秒
	tm := t.Time.Format(time.DateTime)

	return json.Marshal(tm)
}

func (t *LocalTime) UnmarshalJSON(s []byte) (err error) {
	t.Time, err = time.ParseInLocation(time.DateTime, string(s), time.Local)
	if err != nil {
		t.Time, err = time.ParseInLocation("\"2006-01-02 15:04:05\"", string(s), time.Local)
	}
	return
}

func (t LocalTime) Value() (driver.Value, error) {
	return t.Time, nil
}

func (t *LocalTime) Scan(value interface{}) error {
	switch v := value.(type) {
	case []byte:
		return t.UnmarshalJSON(v)
	case string:
		return t.UnmarshalJSON([]byte(v))
	case time.Time:
		*t = LocalTime{Time: v}
	case nil:
		*t = LocalTime{}
	default:
		return fmt.Errorf("can not convert %#v to LocalTime", v)
	}
	return nil
}
