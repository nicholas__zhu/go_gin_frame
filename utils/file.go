/************************
 * Author : nicholas
 * Email  : nicholas.zhu01@gmail.com
 * DATE   : 2023/4/7
 ***********************/

// Package utils ******
package utils

import (
	"bufio"
	"errors"
	"net/url"
	"os"
	"path"
)

//@author: [piexlmax](https://github.com/piexlmax)
//@function: PathExists
//@description: 文件目录是否存在
//@param: path string
//@return: bool, error

func PathExists(path string) (bool, error) {
	fi, err := os.Stat(path)
	if err == nil {
		if fi.IsDir() {
			return true, nil
		}
		return false, errors.New("存在同名文件")
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

//@author: [piexlmax](https://github.com/piexlmax)
//@function: CreateDir
//@description: 批量创建文件夹
//@param: dirs ...string
//@return: err error

func CreateDir(dirs ...string) (err error) {
	for _, v := range dirs {
		exist, err := PathExists(v)
		if err != nil {
			return err
		}
		if !exist {
			if err := os.MkdirAll(v, os.ModePerm); err != nil {
				return err
			}
		}
	}
	return err
}

// IsFileExist 检查文件是否存在
func IsFileExist(filename string) bool {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return false
	}
	return true
}

// DownloadFile 远程下载图片
// @param src string 远程图片地址
// @param dest string 存储的目标地址
func DownloadFile(src, dest string) (string, error) {
	//获取远程文件内容
	re, err := Get(src, nil, nil)
	if err != nil {
		return "", err
	}

	fix := ""
	/*if idx := strings.LastIndex(src, "."); idx != -1 {
		fix = strings.ToLower(src[idx+1:])
	}*/

	urlObj, err := url.Parse(src)
	if err != nil {
		return "", err
	}
	baseQuery := urlObj.Path
	fix = path.Ext(baseQuery)

	if fix == "" {
		return "", errors.New("unknow file type, pic path: " + src)
	}

	thumbF, err := os.OpenFile(dest+fix, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return "", err
	}
	defer func() {
		_ = thumbF.Close()
	}()

	//写入文件
	write := bufio.NewWriter(thumbF)
	_, err = write.WriteString(re)
	if err != nil {
		return "", err
	}
	_ = write.Flush()

	return fix, nil
}

// CopyDataToFile 远程下载图片
// @param data string 需要写入的数据
// @param dest string 存储的目标地址
func CopyDataToFile(data, dest string) error {
	thumbF, err := os.OpenFile(dest, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer func() {
		_ = thumbF.Close()
	}()
	write := bufio.NewWriter(thumbF)
	_, err = write.WriteString(data)
	if err != nil {
		return err
	}
	_ = write.Flush()
	return nil
}
