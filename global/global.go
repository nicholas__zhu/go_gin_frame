package global

import (
	"gitee.com/nicholas__zhu/go_gin_frame/config"
	"github.com/go-redis/redis/v8"
	"sync"

	"go.uber.org/zap"

	"github.com/spf13/viper"
	"gorm.io/gorm"
)

var (
	Db     *gorm.DB
	DbList map[string]*gorm.DB
	Redis  *redis.Client
	Config config.Server
	Viper  *viper.Viper
	Log    *zap.SugaredLogger
	lock   sync.RWMutex
	//GvaTimer              timer.Timer = timer.NewTimerTask()
)

// GetGlobalDBByDBName 通过名称获取db list中的db
func GetGlobalDBByDBName(dbname string) *gorm.DB {
	lock.RLock()
	defer lock.RUnlock()
	return DbList[dbname]
}

// MustGetGlobalDBByDBName 通过名称获取db 如果不存在则panic
func MustGetGlobalDBByDBName(dbname string) *gorm.DB {
	lock.RLock()
	defer lock.RUnlock()
	db, ok := DbList[dbname]
	if !ok || db == nil {
		panic("db no init")
	}
	return db
}
