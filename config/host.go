package config

type Host struct {
	LearnApiHost  string `mapstructure:"learn-api-host" json:"learn-api-host" yaml:"learn-api-host"`    // 学习中心接口地址
	PayApiHost    string `mapstructure:"pay-api-host" json:"pay-api-host" yaml:"pay-api-host"`          // 支付中心接口地址
	EduApiHost    string `mapstructure:"edu-api-host" json:"edu-api-host" yaml:"edu-api-host"`          // 教务接口地址
	StuCdcHost    string `mapstructure:"stu-cdc-host" json:"stu-cdc-host" yaml:"stu-cdc-host"`          // stucdc测评地址
	ReARenApiHost string `mapstructure:"rearen-api-host" json:"rearen-api-host" yaml:"rearen-api-host"` // 人啊人接口地址

	LearnStaticHost string `mapstructure:"learn-static-host" json:"learn-static-host" yaml:"rearen-api-host"` // 学习中心静态资源地址
}
