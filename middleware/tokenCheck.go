/************************
 * Author : nicholas
 * Email  : nicholas.zhu01@gmail.com
 * DATE   : 2023/4/10
 ***********************/

// Package middleware ******
package middleware

import (
	"gitee.com/nicholas__zhu/go_gin_frame/global"
	"gitee.com/nicholas__zhu/go_gin_frame/utils"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

// TokenCheck token判断登录时效
func TokenCheck(stack bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenString := c.Request.Header.Get(utils.HeaderKey)

		// Check if token is missing
		if tokenString == "" || !strings.HasPrefix(tokenString, utils.Pre) {
			c.JSON(http.StatusOK, gin.H{
				"code": 401,
				"data": nil,
				"msg":  "Unauthorized",
			})
			c.Abort()
			return
		}

		loginInfo, err := utils.NewJWT(global.Config.JWT.SigningKey).Decode(
			strings.Replace(tokenString, utils.Pre, "", 1),
		)

		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{
				"code": 401,
				"data": nil,
				"msg":  "Unauthorized",
			})
			c.Abort()
			return
		}

		c.Set("userId", loginInfo.UserId)
		c.Set("nickname", loginInfo.NickName)

		c.Next()
	}
}
