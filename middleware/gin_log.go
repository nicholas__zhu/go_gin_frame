/************************
 * Author : nicholas
 * Email  : nicholas.zhu01@gmail.com
 * DATE   : 2023/4/11
 ***********************/

// Package middleware ******
package middleware

import (
	"bytes"
	"encoding/json"
	"gitee.com/nicholas__zhu/go_gin_frame/global"
	"github.com/gin-gonic/gin"
	"io"
	"strings"
	"time"
)

// LogLayout 日志layout
type LogLayout struct {
	Time      time.Time
	Metadata  map[string]interface{} // 存储自定义原数据
	Path      string                 // 访问路径
	Query     string                 // 携带query
	Body      string                 // 携带body数据
	IP        string                 // ip地址
	UserAgent string                 // 代理
	Error     string                 // 错误
	Cost      time.Duration          // 花费时间 单位 纳秒
	Source    string                 // 来源
}

type Logger struct {
	// Filter 用户自定义过滤
	Filter func(c *gin.Context) bool
	// FilterKeyword 关键字过滤(key)
	FilterKeyword func(layout *LogLayout) bool
	// AuthProcess 鉴权处理
	AuthProcess func(c *gin.Context, layout *LogLayout)
	// 日志处理
	Print func(LogLayout)
	// Source 服务唯一标识
	Source string
}

func (l Logger) SetLoggerMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		path := c.Request.URL.Path
		query := c.Request.URL.RawQuery
		body, _ := c.GetRawData()
		c.Request.Body = io.NopCloser(bytes.NewBuffer(body)) // 将原body塞回去

		c.Next()
		cost := time.Since(start) //单位纳秒 -> 微妙 -> 毫秒 -> 秒
		layout := LogLayout{
			Time:      start,
			Path:      path,
			Query:     query,
			IP:        c.ClientIP(),
			UserAgent: c.Request.UserAgent(),
			Error:     strings.TrimRight(c.Errors.ByType(gin.ErrorTypePrivate).String(), "\n"),
			Cost:      cost,
			Source:    l.Source,
			Body:      string(body),
		}
		/*if l.Filter != nil && !l.Filter(c) {
			layout.Body = string(body)
		}*/
		/*if l.AuthProcess != nil {
			// 处理鉴权需要的信息
			l.AuthProcess(c, &layout)
		}*/
		/*if l.FilterKeyword != nil {
			// 自行判断key/value 脱敏等
			l.FilterKeyword(&layout)
		}*/

		l.Print(layout) // 自行处理日志
	}
}

func DefaultLogger() gin.HandlerFunc {
	return Logger{
		Print: func(layout LogLayout) {
			// 标准输出,k8s做收集
			v, _ := json.Marshal(layout)
			//fmt.Println(string(v))
			global.Log.Info(string(v))
		},
		Source: "gin_request",
	}.SetLoggerMiddleware()
}
