/************************
 * Author : nicholas
 * Email  : nicholas.zhu01@gmail.com
 * DATE   : 2023/4/10
 ***********************/

// Package middleware ******
package middleware

import (
	"encoding/json"
	"gitee.com/nicholas__zhu/go_gin_frame/global"
	"github.com/gin-gonic/gin"
	"net"
	"net/http"
	"net/http/httputil"
	"os"
	"runtime/debug"
	"strings"
)

// GinRecovery recover掉项目可能出现的panic，并使用zap记录相关日志
func GinRecovery(stack bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				var brokenPipe bool //判断网络是否断开
				if ne, ok := err.(*net.OpError); ok {
					if se, ok := ne.Err.(*os.SyscallError); ok {
						if strings.Contains(strings.ToLower(se.Error()), "broken pipe") || strings.Contains(strings.ToLower(se.Error()), "connection reset by peer") {
							brokenPipe = true
						}
					}
				}

				httpRequest, _ := httputil.DumpRequest(c.Request, false)
				var logInfo map[string]interface{}
				logInfo = map[string]interface{}{
					"url":     c.Request.URL.Path,
					"error":   err,
					"request": string(httpRequest),
					"msg":     "系统内部错误",
				}
				if stack {
					logInfo["debugStack"] = string(debug.Stack())
				}
				if brokenPipe {
					logInfo["msg"] = "网络断开错误"
				}

				panicLog, _ := json.Marshal(logInfo)
				global.Log.Errorf(string(panicLog))

				if brokenPipe {
					_ = c.Error(err.(error))
					c.Abort()
					return
				}
				c.AbortWithStatus(http.StatusInternalServerError)
			}
		}()
		c.Next()
	}
}
