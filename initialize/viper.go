/************************
 * Author : nicholas
 * Email  : nicholas.zhu01@gmail.com
 * DATE   : 2023/4/6
 ***********************/

// Package initialize ******
package initialize

import (
	"fmt"
	"gitee.com/nicholas__zhu/go_gin_frame/global"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

// ViperInit Viper初始化
func ViperInit() {
	config := "config.yaml"
	fmt.Printf("config的路径为%s\n", config)

	v := viper.New()
	v.SetConfigFile(config)
	v.SetConfigType("yaml")
	err := v.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	v.WatchConfig()

	v.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("config file changed:", e.Name)
		if err = v.Unmarshal(&global.Config); err != nil {
			fmt.Println(err)
		}
	})
	if err = v.Unmarshal(&global.Config); err != nil {
		fmt.Println(err)
	}
}
