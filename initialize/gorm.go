package initialize

import (
	"gitee.com/nicholas__zhu/go_gin_frame/global"
)

// GormInit Gorm 初始化数据库并产生数据库全局变量
func GormInit() {
	switch global.Config.System.DbType {
	case "mysql":
		global.Db = GormMysql()
	case "pgsql":
		global.Db = GormPgSql()
	case "oracle":
		global.Db = GormOracle()
	case "mssql":
		global.Db = GormMssql()
	default:
		global.Db = GormMysql()
	}
}
