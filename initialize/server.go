/************************
 * Author : nicholas
 * Email  : nicholas.zhu01@gmail.com
 * DATE   : 2023/4/7
 ***********************/

// Package initialize ******
package initialize

import (
	"context"
	"fmt"
	"gitee.com/nicholas__zhu/go_gin_frame/global"
	"gitee.com/nicholas__zhu/go_gin_frame/middleware"
	"github.com/gin-gonic/gin"
	"net/http"
)

type initRouter func(ginRouter *gin.Engine)

func RunWindowsServer(initRout initRouter, ctx context.Context) {

	switch global.Config.System.Env {
	case "public":
		gin.SetMode(gin.DebugMode)
	case "test":
		gin.SetMode(gin.TestMode)
	case "release":
		gin.SetMode(gin.ReleaseMode)
	default:
		gin.SetMode(gin.DebugMode)
	}

	ginRouter := gin.New()
	ginRouter.Use(middleware.GinRecovery(true))
	ginRouter.Use(middleware.DefaultLogger())
	//初始化路由
	initRout(ginRouter)

	//初始化服务器
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", global.Config.System.Addr),
		Handler: ginRouter,
	}

	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		global.Log.Errorf("listen: %s\n", err)
	}

	select {
	case <-ctx.Done():
		if err := srv.Shutdown(ctx); err != nil {
			global.Log.Error("Server Shutdown:" + err.Error())
		}

	}
}
