/************************
 * Author : nicholas
 * Email  : nicholas.zhu01@gmail.com
 * DATE   : 2023/4/7
 ***********************/

// Package initialize ******
package initialize

import (
	"gitee.com/nicholas__zhu/go_gin_frame/middleware"
	"github.com/gin-gonic/gin"
)

func Routers() *gin.Engine {
	//ginRouter := gin.Default()
	ginRouter := gin.New()
	//engine.Use(Logger(), Recovery())

	ginRouter.Use(middleware.GinRecovery(true))
	ginRouter.Use(middleware.DefaultLogger())

	return ginRouter
}
