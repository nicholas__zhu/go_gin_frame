/************************
 * Author : nicholas
 * Email  : nicholas.zhu01@gmail.com
 * DATE   : 2023/4/7
 ***********************/

// Package initialize ******
package initialize

import (
	"context"
	"fmt"
	"gitee.com/nicholas__zhu/go_gin_frame/global"
	"github.com/go-redis/redis/v8"
)

func RedisInit() {
	redisCfg := global.Config.Redis
	if !redisCfg.Open {
		global.Log.Info("redis未被启用 ---- ×")
		global.Redis = nil
		return
	}
	client := redis.NewClient(&redis.Options{
		Addr:     redisCfg.Addr,
		Password: redisCfg.Password, // no password set
		DB:       redisCfg.DB,       // use default DB
	})
	pong, err := client.Ping(context.Background()).Result()
	if err != nil {
		global.Log.Error("redis connect ping failed, err:", err.Error())
	} else {
		global.Log.Info("redis connect ping response:", fmt.Sprintf("%v", pong))
		global.Redis = client
	}
}
