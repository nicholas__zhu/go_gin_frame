/************************
 * Author : nicholas
 * Email  : nicholas.zhu01@gmail.com
 * DATE   : 2023/4/7
 ***********************/

// Package initialize ******
package initialize

import (
	"gitee.com/nicholas__zhu/go_gin_frame/global"
	"gitee.com/nicholas__zhu/go_gin_frame/utils"
)

func OtherInit() {
	_, err := utils.ParseDuration(global.Config.JWT.ExpiresTime)
	if err != nil {
		panic(err)
	}
}
