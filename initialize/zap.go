/************************
 * Author : nicholas
 * Email  : nicholas.zhu01@gmail.com
 * DATE   : 2023/4/7
 ***********************/

// Package initialize ******
package initialize

import (
	"fmt"
	"gitee.com/nicholas__zhu/go_gin_frame/global"
	"gitee.com/nicholas__zhu/go_gin_frame/utils"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"os"
	"time"
)

// ZapInit Zap初始化
func ZapInit() {
	global.Log = newZapLog()
}

func newZapLog() *zap.SugaredLogger {
	if ok, _ := utils.PathExists(global.Config.Zap.Director); !ok { // 判断是否有Director文件夹
		fmt.Printf("create %v directory\n", global.Config.Zap.Director)
		err := os.MkdirAll(global.Config.Zap.Director, os.ModePerm)
		if err != nil {
			panic(err.Error())
		}
	}
	core := zapcore.NewCore(getEncoder(), getWriterSyncer(), global.Config.Zap.TransportLevel())
	return zap.New(core, zap.AddCaller()).Sugar()
}

func getEncoder() zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.TimeKey = "time"
	encoderConfig.EncodeLevel = zapcore.LowercaseLevelEncoder
	encoderConfig.EncodeTime = func(t time.Time, encoder zapcore.PrimitiveArrayEncoder) {
		encoder.AppendString(t.Local().Format(time.DateTime + ".000"))
	}
	return zapcore.NewJSONEncoder(encoderConfig)
}

func getWriterSyncer() zapcore.WriteSyncer {
	now := time.Now()
	lumberJackSyncer := &lumberjack.Logger{
		Filename:   fmt.Sprintf("%s/%04d%02d%02d.log", global.Config.Zap.Director, now.Year(), now.Month(), now.Day() /*now.Hour(), now.Minute(), now.Second()*/),
		MaxSize:    100,                      // 每个日志文件保存的最大尺寸
		MaxBackups: 3,                        //日志文件最多保存多少个备份
		MaxAge:     global.Config.Zap.MaxAge, // 文件最多保存多少天
		Compress:   true,                     // 是否压缩
	}

	if global.Config.Zap.LogInConsole {
		return zapcore.NewMultiWriteSyncer(zapcore.AddSync(lumberJackSyncer), os.Stdout)
	}
	return zapcore.AddSync(lumberJackSyncer)
}
